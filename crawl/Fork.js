"use strict";

var Cluster	= require('cluster');

(function(self) {

    var CallbackList = {};

	var _init = function() {
        var ClusterConfiguration = {};
        ClusterConfiguration.exec = './crawl/Worker.js';
        ClusterConfiguration.args = ['--use', 'http'];

        Cluster.setupMaster(ClusterConfiguration);
        Cluster.on('message', HandleWorkerMessage);
	};

    var _startWorker = function(Request, Callback) {
        CallbackList[Request.Hash] = Callback;

        var Worker = Cluster.fork(GetEnvironment(Request));
        Worker.send(Request);
    };

    var HandleWorkerMessage = function(Worker, Request) {
        var Callback = GetCallbackByHash(Request.Hash);

        Worker.disconnect();

        if (Request.Success) {
            Callback(Request.Data, Request.StatusCode, null);
        } else {
            Callback(null, Request.StatusCode, Request.Data);
        }
    };

    var GetEnvironment = function(Request) {
        var Environment         = {};
        Environment.Language    = Request.Language;

        return Environment;
    };

    var GetCallbackByHash = function(Hash) {
        var Callback = CallbackList[Hash];
        if (Callback !== null) {
            delete CallbackList[Hash];
        }

        return Callback;
    };

	self.Init          = _init;
    self.StartWorker   = _startWorker;
})(this);