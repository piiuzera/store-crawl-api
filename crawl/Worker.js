"use strict";

var LaVilleMarrieListCrawlService    = require('./worker/services/LaVilleMarrieListCrawlService');
var CasasBahiaMarrieListCrawlService  = require('./worker/services/CasasBahiaMarrieListCrawlService');

var EnvConfig   = require('../environment/EnvConfig');
var HttpStatus  = require('../enums/HttpStatus');
var RequestType = require('./worker/config/RequestType');
var Utils       = require('../environment/Utils');

(function(self) {

	var _init = function() {
		process.on('message', HandleForkMessage);
        EnvConfig.Init();
        EnvConfig.SetLanguage(process.env['Language']);
	};

    var HandleForkMessage = function(Request) {
        if (IsRequestLaVilleMarrieList(Request)) {
            GetLaVilleMarrieList(Request);
        } else if (IsRequestCasasBahiaMarrieList(Request)) {
            GetCasasBahiaMarrieList(Request);
        } else {
            var Error = Utils.GetThrowException(__('InternalServerError'), 'InternalServerError');
            Send(false, Request, Error);
            
            return;
        }
    };

    var GetLaVilleMarrieList = function(Request) {
        LaVilleMarrieListCrawlService.Init(
            Request.IdMarrieList,
            CallbackCrawlResponse.bind(this, Request)
        );
    };

    var GetCasasBahiaMarrieList = function(Request) {
        CasasBahiaMarrieListCrawlService.Init(
            Request.IdMarrieList,
            CallbackCrawlResponse.bind(this, Request)
        );
    };

    var CallbackCrawlResponse = function(Request, ResponseObject, StatusCode, Error) {
        Request.StatusCode = StatusCode;
    	if (Error) {
    		Send(false, Request, Error);
    	} else {
            Send(true, Request, ResponseObject);
        }
    };

    var Send = function(Success, Request, Data) {
        Request.Success = Success;
        Request.Data = Data;

        process.send(Request);
    };


    var IsRequestLaVilleMarrieList = function(Request) {
        return Request.Type === RequestType.LA_VILLE_MARRIE_LIST_REQUEST_TYPE;
    };

    var IsRequestCasasBahiaMarrieList = function(Request) {
        return Request.Type === RequestType.CASAS_BAHIA_MARRIE_LIST_REQUEST_TYPE;
    };

	self.Init = _init;
})(this);

this.Init();