'use strict';

var FieldsModel 		= require('../../../enums/FieldsModel');
var FieldsRequest 		= require('../../../enums/FieldsRequest');
var HttpStatus 			= require('../../../enums/HttpStatus');
var JsonFile 			= require('jsonfile');
var Log 				= require('../../../config/Log');
var Structure 			= require('../config/Structure');
var Utils 				= require('../../../environment/Utils');
var Web 				= require('../config/Web');
var WorkerConstants 	= require('../WorkerConstants');

(function(self) {

	var MarrieListObjectExist = false;

	var _init = function(IdMarrieList, Callback) {
		Log.Info('GET CASAS BAHIA MARRIE LIST STARTED');

		MarrieListObjectExist = GetMarrieListObjectExist(IdMarrieList);

		Web.Get(
			(GetMarrieListUrl(IdMarrieList) + GetMarrieListRequestQuery()),
			CallbackCasasBahiaMarrieListRequest.bind(this, IdMarrieList, Callback)
		);
	};

	var CallbackCasasBahiaMarrieListRequest = function(IdMarrieList, Callback, Body, Status) {
		try {
			ThrowCasasBahiaMarrieListRequestError(Status);

			var MarrieListObject = {};
			if (Status > 399) {
				MarrieListObject = MarrieListObjectExist;
			} else {
				MarrieListObject = GetMarrieListObject(Body, IdMarrieList);
			}

			Log.Success('GET CASAS BAHIA MARRIE LIST SUCCESS', MarrieListObject);

			Callback(MarrieListObject, HttpStatus.HTTP_STATUS_SUCCESS, null);
		} catch (ex) {
			Log.Error('GET CASAS BAHIA MARRIE LIST ERROR', ex);
			Callback(null, (ex.status || HttpStatus.HTTP_STATUS_BAD_REQUEST), ex);
		}
	};

	var ThrowCasasBahiaMarrieListRequestError = function(Status) {
		if (Status > 399 && !MarrieListObjectExist) {
			throw Utils.GetThrowException(__('CasasBahiaError'), 'CasasBahiaError');
		}
	};

	var GetMarrieListRequestQuery = function() {
		var RequestQuery = {};
		RequestQuery[FieldsRequest.CASAS_BAHIA_ITEMS_PER_PAGE] 	= WorkerConstants.CASAS_BAHIA_MARRIE_LIST_ITEM_PER_PAGE;
		RequestQuery[FieldsRequest.CASAS_BAHIA_PAGE_NUMBER] 	= WorkerConstants.CASAS_BAHIA_MARRIE_LIST_PAGE_INDEX;
		RequestQuery[FieldsRequest.CASAS_BAHIA_CONTEXT] 		= WorkerConstants.CASAS_BAHIA_MARRIE_LIST_CONTEXT;

		return Utils.JsonToQueryString(RequestQuery);
	};

	var GetMarrieListObject = function(Body, IdMarrieList) {
		var MarrieListObject = {};
		MarrieListObject[FieldsModel.MARRIE_LIST_PRODUCTS_COUNT] 	= GetItensFound(Body);
		MarrieListObject[FieldsModel.MARRIE_LIST_PRODUCTS] 		= [];

		for (var i = 0; i < Body.Itens.length; ++i) {
			var Product = Body.Itens[i];
			MarrieListObject[FieldsModel.MARRIE_LIST_PRODUCTS].unshift(
				GetProductDetails(Product, IdMarrieList)
			);
		}

		return MarrieListObject;
	};

	var GetItensFound = function(Body) {
		return Body.QuantidadeProdutos;
	};

	var GetProductDetails = function(Product, IdMarrieList) {
		var ProductDetails = {};
		ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_ID] 			= GetProductId(Product);
		ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_NAME] 		= GetProductName(Product);
		ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_PICTURE]		= GetProductPicture(Product);
		ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_PRICE]		= GetProductPrice(Product);
		ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_BOUGHT]		= GetProductBought(Product);
		ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_ADD_CART]	= GetProductAddCart(Product, IdMarrieList);
		ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_BUYED]		= GetProductBuyed(Product);

		return ProductDetails;
	};

	var GetProductId = function(Product) {
		return Product.IdSku;
	};

	var GetProductName = function(Product) {
		return Product.DetalheProduto.Nome;
	};

	var GetProductPicture = function(Product) {
		var ProductPicture = null;
		for (var i = 0; i < Product.DetalheProduto.ListaImagens.length; ++i) {
			var Picture = Product.DetalheProduto.ListaImagens[i];
			if (Picture.Largura === WorkerConstants.CASAS_BAHIA_MARRIE_LIST_WIDTH_PICTURE &&
				Picture.Altura === WorkerConstants.CASAS_BAHIA_MARRIE_LIST_HEIGHT_PICTURE) {
				ProductPicture = Picture.Url;
			}
		}

		return ProductPicture;
	};

	var GetProductPrice = function(Product) {
		return Product.Preco ? Product.Preco.PrecoVenda.PrecoSemDesconto : 0;
	};

	var GetProductBought = function(Product) {
		return Product.QuantidadeItemComprado;
	};

	var GetProductAddCart = function(Product, IdMarrieList) {
		var ProductAddCart 	= Structure.HttpCasasBahiaCart.replace('{{productId}}', Product.IdSku);
		ProductAddCart 		= ProductAddCart.replace('{{storeId}}', Product.IdLojista);
		ProductAddCart 		= ProductAddCart.replace('{{idMarrieList}}', IdMarrieList);

		return ProductAddCart;
	};

	var GetProductBuyed = function(Product) {
		return Product.QuantidadeDesejada === Product.QuantidadeItemComprado;
	};

	var GetMarrieListUrl = function(IdMarrieList) {
		return Structure.HttpCasasBahiaMarrieList.replace('{{idMarrieList}}', IdMarrieList);
	};

	var GetMarrieListObjectExist = function(IdMarrieList) {
		try {
			return JsonFile.readFileSync('./files/casasBahia' + IdMarrieList + '.json');
		} catch (ex) {
			return false;
		}
	};

	self.Init = _init;
})(this);