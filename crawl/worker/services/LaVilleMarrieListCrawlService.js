'use strict';

var Cheerio 			= require('cheerio');
var FieldsModel 		= require('../../../enums/FieldsModel');
var FieldsRequest 		= require('../../../enums/FieldsRequest');
var HttpStatus 			= require('../../../enums/HttpStatus');
var Log 				= require('../../../config/Log');
var Structure 			= require('../config/Structure');
var Utils 				= require('../../../environment/Utils');
var Web 				= require('../config/Web');
var WorkerConstants 	= require('../WorkerConstants');

(function(self) {

	var $ = {};

	var _init = function(IdMarrieList, Callback) {
		Log.Info('GET LA VILLE MARRIE LIST STARTED');

		Web.Get(
			(GetMarrieListUrl(IdMarrieList) + GetMarrieListRequestQuery()),
			CallbackLaVilleMarrieListRequest.bind(this, Callback)
		);
	};

	var CallbackLaVilleMarrieListRequest = function(Callback, Body, Status) {
		try {
			$ = Cheerio.load(Body);
			ThrowLaVilleMarrieListRequestError(Status);
			var MarrieListObject = GetMarrieListObject();

			Log.Success('GET LA VILLE MARRIE LIST SUCCESS', MarrieListObject);

			Callback(MarrieListObject, HttpStatus.HTTP_STATUS_SUCCESS, null);
		} catch (ex) {
			Log.Error('GET LA VILLE MARRIE LIST ERROR', ex);
			Callback(null, (ex.status || HttpStatus.HTTP_STATUS_BAD_REQUEST), ex);
		}
	};

	var ThrowLaVilleMarrieListRequestError = function(Status) {
		if (Status > 399) {
			throw Utils.GetThrowException(__('LaVilleError'), 'LaVilleError');
		}
	};

	var GetMarrieListRequestQuery = function() {
		var RequestQuery = {};
		RequestQuery[FieldsRequest.LA_VILLE_ITEMS_PER_PAGE] = WorkerConstants.LA_VILLE_MARRIE_LIST_ITEM_PER_PAGE;
		RequestQuery[FieldsRequest.LA_VILLE_PAGE_NUMBER] 	= WorkerConstants.LA_VILLE_MARRIE_LIST_PAGE_INDEX;

		return Utils.JsonToQueryString(RequestQuery);
	};

	var GetMarrieListObject = function() {
		var MarrieListObject = {};
		MarrieListObject[FieldsModel.MARRIE_LIST_PRODUCTS_COUNT] 	= GetItensFound();
		MarrieListObject[FieldsModel.MARRIE_LIST_PRODUCTS] 			= [];

		$('div[id^=ResultItems_]').find('ul').each(function(IndexRow) {
			var Row = this;
			$(this).find('li').each(function(IndexCell) {
				var Cell = this;
				var ProductDetails = GetProductDetails(Row, Cell);
				if (ProductDetails) {
					MarrieListObject[FieldsModel.MARRIE_LIST_PRODUCTS].unshift(GetProductDetails(Row, Cell));
				}
			});
		});

		return MarrieListObject;
	};

	var GetItensFound = function() {
		var ItensFoundStr = Utils.StringValidOrEmpty($('.searchResultsTime').first().find('.resultado-busca-numero').find('.value').text());
		return ItensFoundStr ? Number(ItensFoundStr) : 0;
	};

	var GetProductDetails = function(Row, Cell) {
		var ProductDetails = null;
		var ProductId = GetProductId(Cell);
		if (ProductId) {
			ProductDetails = {};
			ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_ID] 			= ProductId;
			ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_NAME] 		= GetProductName(Cell);
			ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_PICTURE]		= GetProductPicture(Cell);
			ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_PRICE]		= GetProductPrice(Cell);
			ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_BOUGHT]		= GetProductBought(Cell);
			ProductDetails[FieldsModel.MARRIE_LIST_PRODUCT_ADD_CART]	= GetProductAddCart(Cell, ProductId);

		}

		return ProductDetails;
	};

	var GetProductId = function(Cell) {
		var ProductId = Utils.StringValidOrEmpty($(Cell).find('.refProd li').text());
		return ProductId ? Number(ProductId) : null;
	};

	var GetProductName = function(Cell) {
		return Utils.StringValidOrEmpty($(Cell).find('.nome').text());
	};

	var GetProductPicture = function(Cell) {
		return Utils.StringValidOrEmpty($(Cell).find('.fotoProduto').find('img').attr('src'));
	};

	var GetProductPrice = function(Cell) {
		var ProductPrice = Utils.StringValidOrEmpty($(Cell).find('.preco').text());
		ProductPrice = ProductPrice.replace('R$', '');
		ProductPrice = ProductPrice.replace(',', '.');
		return parseFloat(ProductPrice);
	};

	var GetProductBought = function(Cell) {
		var ProductBought = Utils.StringValidOrEmpty($(Cell).find('.produtos-disponiveis .comprados b').text());
		return ProductBought ? Number(ProductBought) : null;
	};

	var GetProductAddCart = function(Cell, ProductId) {
		return Utils.StringValidOrEmpty($(Cell).find('#' + ProductId).find('a').attr('href'));
	};

	var GetMarrieListUrl = function(IdMarrieList) {
		return Structure.HttpLaVilleMarrieList.replace('{{idMarrieList}}', IdMarrieList);
	};

	self.Init = _init;
})(this);