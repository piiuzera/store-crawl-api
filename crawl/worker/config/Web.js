"use strict";

var RequestConnection   = require('axios');
var HttpStatus          = require('../../../enums/HttpStatus');
var Utils               = require('../../../environment/Utils');

(function() {

    var Request = RequestConnection.create({
        timeout: 15000
    });

    var _requestCallback = function(url, callback, retryCount, response) {
        var statusCode = response && response.status ? response.status : HttpStatus.HTTP_STATUS_FORBIDDEN;
        var headers = response && response.headers ? response.headers : {};
        var body = response && response.data ? response.data : null;

        if (response instanceof Error) {
            var ex = Utils.GetThrowException(__('ServerNotRunning'), 'ServerNotRunning', response.message, statusCode);
            callback(JSON.stringify(ex), statusCode, headers, response);
        } else {
            callback(body, statusCode, headers, response);
        }
    };


    var _get = function(url, callback, headers, _retryCount) {
        var getOptions = {
            'headers' : {}
        };

        setHeaders(getOptions, headers);

        Request.get(
            url,
            getOptions,
        )
        .then(_requestCallback.bind(null, url, callback, _retryCount))
        .catch(_requestCallback.bind(null, url, callback, _retryCount));

    };

    var _post = function(url, form, callback, headers, _retryCount) {
        var postOptions = {
            'headers' : {}
        };

        setHeaders(postOptions, headers);

        Request.post(
            url,
            form,
            postOptions
        )
        .then(_requestCallback.bind(null, url, callback, _retryCount))
        .catch(_requestCallback.bind(null, url, callback, _retryCount));
    };

    var setHeaders = function(options, headers) {
        if (headers && Object.keys(headers).length > 0) {
            for (var key in headers) {
                options.headers[key] = headers[key];
            }
        }
    };

    module.exports.Get  = _get;
    module.exports.Post = _post;
})();
