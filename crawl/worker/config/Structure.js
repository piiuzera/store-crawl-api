'use strict';

(function(self) {
	var baseLaVilleUrl				= 'https://www.lavillecasa.com.br';
	var baseCasasBahiaUrl			= 'https://api-b2c.casasbahia.com.br';

	var _httpLaVilleMarrieList 		= baseLaVilleUrl + '/list/{{idMarrieList}}?';
	var _httpCasasBahiaMarrieList	= baseCasasBahiaUrl + '/Gateway/V1/Listas/{{idMarrieList}}/Itens?';
	var _httpCasasBahiaCart 		= 'https://carrinho.casasbahia.com.br/?idSku={{productId}}&idLojista={{storeId}}&IdListaDeCompras={{idMarrieList}}';

	self.HttpLaVilleMarrieList		= _httpLaVilleMarrieList;
	self.HttpCasasBahiaMarrieList	= _httpCasasBahiaMarrieList;
	self.HttpCasasBahiaCart 		= _httpCasasBahiaCart;
})(this);