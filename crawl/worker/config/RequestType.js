'use strict';

(function(self) {
	self.LA_VILLE_MARRIE_LIST_REQUEST_TYPE		= 'LaVilleMarrieListRequestType';
	self.CASAS_BAHIA_MARRIE_LIST_REQUEST_TYPE	= 'CasasBahiaMarrieListRequestType';
})(this);