'use strict';

(function(self) {
	var _deprecated = function(Request, Response, Next) {
		Response.IsDeprecated = true;
		Next();
	};

	self.Deprecated = _deprecated;
})(this);