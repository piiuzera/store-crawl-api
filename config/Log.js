"use strict";

var Colors = require('colors');
var FS = require('fs');
var Utils = require('../environment/Utils');

(function(self) {

    var LogAccessFile   = {};

    var _init = function() {
        LogAccessFile       = GetFileLog('access');

        Colors.setTheme({
            success : 'green',
            info    : 'gray',
            error   : 'red'
        });
    };
    
    var _error = function(message, error) {
        GetConsoleLog(
            (Colors.success('[ ' + GetActualDateFormat() + ' ][ PID: ' + process.pid + ' ]') + Colors.error('[ ERROR ] ') + message),
            error,
            LogAccessFile            
        );
    };

    var _info = function(message, obj) {
        GetConsoleLog(
            (Colors.success('[ ' + GetActualDateFormat() + ' ][ PID: ' + process.pid + ' ]') + Colors.info('[ INFO  ] ') + message),
            obj,
            LogAccessFile
        );
    };

    var _success = function(message, obj) {
        GetConsoleLog(
            (Colors.success('[ ' + GetActualDateFormat() + ' ][ PID: ' + process.pid + ' ]') + Colors.success('[SUCCESS] ') + message),
            obj,
            LogAccessFile
        );
    };

    var GetActualDateFormat = function() {
        var ActualDate = new Date();
        var ActualDateFormat = Utils.GetDateFormat(ActualDate, 'YYYY-MM-DD HH:mm:ss');

        return ActualDateFormat;
    };

    var GetFileLog = function(Filename) {
        var folder  = (process.cwd() + '/logs');
        var path    = (folder + '/' + Filename + '.log');

        if (!FS.existsSync(folder)) {
            FS.mkdirSync(folder);
        }

        if (!FS.existsSync(path)) {
            FS.writeFileSync(path, '');
        }

        return FS.createWriteStream(path, GetFileLogOptions());
    };

    var GetFileLogOptions = function() {
        var FileLogOptions = {};
        FileLogOptions.flags = 'a';

        return FileLogOptions;
    };

    var GetConsoleLog = function(Text, Obj, File) {
        if (File) {
            File.write(Text + '\n');
            if (Obj) {
                File.write(JSON.stringify(Obj) + '\n');
            }
        }
        return console.log(Text);
    };

    self.Init         = _init;
    self.Error        = _error;
    self.Info         = _info;
    self.Success      = _success;
})(this);

this.Init();