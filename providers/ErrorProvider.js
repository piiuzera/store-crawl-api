'use strict';

var HttpStatus 	= require('../enums/HttpStatus');
var Log 		= require('../config/Log');
var Utils 		= require('../environment/Utils');

(function(self) {

	var _init = function() {
		process.on(
			'uncaughtException',
			Handler.bind(this)
		);
	};

	var Handler = function(Error) {
		var HttpResponse 		= Utils.GetHttpResponseErrorModel(HttpStatus.HTTP_STATUS_BAD_REQUEST);
		HttpResponse.message 	= __('InternalServerError');
		HttpResponse.code 		= 'InternalServerError';
	};

	self.Init = _init;
})(this);