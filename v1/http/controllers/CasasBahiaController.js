'use strict';

var Fork 		= require('../../../crawl/Fork');
var HttpMethod = require('../../../enums/HttpMethod');
var RequestType = require('../../../crawl/worker/config/RequestType');
var Utils 		= require('../../../environment/Utils');

(function(self) {

	var BaseRoute = '/casasBahia';
	var Routes 	  = [];

	var _init = function() {
		Utils.AddRoute(Routes, BaseRoute, HttpMethod.GET, '/marrieList/:IdMarrieList', GetMarrieList);
	};

	var GetMarrieList = function(Request, Response) {
		var HttpResponse 	= Utils.GetHttpResponseModel();
		var RequestObject 	= GetMarrieListRequestObject(Request);

		Fork.StartWorker(
			RequestObject,
			CallbackMarrieList.bind(this, HttpResponse, Request, Response)
		);
	};

	var CallbackMarrieList = function(HttpResponse, Request, Response, MarrieListObject, StatusCode, Error) {
		if (Error) {
			Utils.GetReturnsError(HttpResponse, Error, StatusCode);
		} else {
			HttpResponse.data = MarrieListObject;
			HttpResponse.status = StatusCode;
		}
		Utils.GetReturnsResponse(Request, Response, HttpResponse);
	};

	var GetMarrieListRequestObject = function(Request) {
		var RequestObject 			= {};
		RequestObject.Hash 			= Utils.GenerateRequestHash();
		RequestObject.Type 			= RequestType.CASAS_BAHIA_MARRIE_LIST_REQUEST_TYPE;
		RequestObject.Language 		= Request.language;
		RequestObject.IdMarrieList 	= Request.params.IdMarrieList;

		return RequestObject;
	};

	var _getRoutes = function() {
		return Routes;
	};

	self.Init = _init;
	self.GetRoutes = _getRoutes;
})(this);

this.Init();