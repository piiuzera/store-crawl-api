'use strict';

var Controller = require('../controllers/LaVilleController');
var DeprecatedMiddleware = require('../../../middlewares/DeprecatedMiddleware');

var Express = require('express');

(function() {

	var _route = function(IsActualRouterVersion) {
		var Router = Express.Router();

		Controller.GetRoutes().forEach(function(Route) {
			if (IsNotActualRouterVersion(IsActualRouterVersion)) {
				Router[Route.method](Route.uri, DeprecatedMiddleware.Deprecated, Route.callback);
			} else {
				Router[Route.method](Route.uri, Route.callback);
			}
		});

		return Router;
	};

	var IsNotActualRouterVersion = function(IsActualRouterVersion) {
		return !IsActualRouterVersion;
	};

	module.exports = _route;
})();