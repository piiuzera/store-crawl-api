'use strict';

var HttpProvider = require('./providers/HttpProvider');

(function(self) {

	var _init = function(Router, IsActualRouterVersion) {
		HttpProvider.Init(Router, IsActualRouterVersion);
	};

	self.Init = _init;

})(this);