'use strict';

var IndexRoute 		= require('../http/routes/IndexRoute');
var LaVilleRoute 	= require('../http/routes/LaVilleRoute');
var CasasBahiaRoute	= require('../http/routes/CasasBahiaRoute');

(function(self) {

	var _initializeRoute = function(Router, IsActualRouterVersion) {
		/*
		* Routes
		*/
		Router.use('/', 			IndexRoute(IsActualRouterVersion));
		Router.use('/laVille', 		LaVilleRoute(IsActualRouterVersion));
		Router.use('/casasBahia', 	CasasBahiaRoute(IsActualRouterVersion));
	};

	self.InitializeRoute = _initializeRoute;
})(this);