'use strict';

var RouteProvider = require('./RouteProvider');

(function(self) {

	var _init = function(Router, IsActualRouterVersion) {
		RouteProvider.InitializeRoute(Router, IsActualRouterVersion);
	};

	self.Init = _init;
})(this);