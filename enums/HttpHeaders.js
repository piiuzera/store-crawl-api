'use strict';

(function(self) {
	/**
	* DEFAULT HEADERS VALUE
	**/
	self.ACCEPT_LANGUAGE 	= 'accept-language';
	self.COOKIE 			= 'cookie';
	self.AUTHORIZATION 		= 'authorization';
})(this);