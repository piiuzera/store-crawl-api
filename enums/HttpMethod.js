'use strict';

(function(self) {
	self.GET 	= 'get';
	self.POST 	= 'post';
	self.PUT 	= 'put';
	self.DELETE = 'delete'
})(this);