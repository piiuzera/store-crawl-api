'use strict';

(function(self) {
	self.LA_VILLE_ITEMS_PER_PAGE 	= 'PS';
	self.LA_VILLE_PAGE_NUMBER 		= 'PageNumber';

	self.CASAS_BAHIA_ITEMS_PER_PAGE = 'RegistrosPorPagina';
	self.CASAS_BAHIA_PAGE_NUMBER 	= 'Pagina';
	self.CASAS_BAHIA_CONTEXT 		= 'Contexto';
})(this);