'use strict';

(function(self) {
	self.MARRIE_LIST_PRODUCT_ADD_CART	= 'addCart';
	self.MARRIE_LIST_PRODUCT_BOUGHT		= 'bought';
	self.MARRIE_LIST_PRODUCT_BUYED 		= 'buyed';
	self.MARRIE_LIST_PRODUCT_ID 		= 'id';
	self.MARRIE_LIST_PRODUCT_NAME 		= 'name';
	self.MARRIE_LIST_PRODUCT_PICTURE	= 'picture';
	self.MARRIE_LIST_PRODUCT_PRICE		= 'price';
	self.MARRIE_LIST_PRODUCTS 			= 'products';
	self.MARRIE_LIST_PRODUCTS_COUNT 	= 'productsCount';
})(this);