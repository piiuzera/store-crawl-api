'use strict';

var DotEnv = require('dotenv');
var Express = require('express');
var Glob = require('glob');
var ResourceConfig = require('i18n');

(function(self) {

	var _init = function() {
		DotEnv.config();
		ConfigureResourceConfig();
	};

	var ConfigureResourceConfig = function() {
		var Configuration = {};
		Configuration.locales = ['en', 'es', 'pt'];
		Configuration.directory = process.cwd() + '/strings';
		Configuration.register = global;
		
		ResourceConfig.configure(Configuration);
	};

	var _setLanguage = function(Language) {
		ResourceConfig.setLocale(Language);
	};

	var _getStartServer = function() {
		var StartServers = {};
		var Versions = GetVersions();
		for (var i = 0; i < Versions.length; ++i) {
			var Version = Versions[i];
			var StartServer = require('../' + Version + '/StartServer');
			StartServers[Version] = StartServer;
		}
		return StartServers;
	};

	var _getRouterVersion = function() {
		var RouterVersion = {};
		var Versions = GetVersions();
		for (var i = 0; i < Versions.length; ++i) {
			var Version = Versions[i];
			var Router = Express.Router();
			RouterVersion[Version] = Router;
		}

		return RouterVersion;
	};

	var GetVersions = function() {
		return Glob.sync('v*');
	};

	self.Init = _init;
	self.SetLanguage = _setLanguage;
	self.GetStartServer = _getStartServer;
	self.GetRouterVersion = _getRouterVersion;
})(this);

this.Init();