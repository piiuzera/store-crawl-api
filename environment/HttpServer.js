'use strict';

var BodyParser = require('body-parser');
var Cors = require('cors');
var ErrorProvider = require('../providers/ErrorProvider');
var Express = require('express');
var NotFoundProvider = require('../providers/NotFoundProvider');
var ResourceConfig = require('i18n');

(function(self) {

	var App = Express();

	var _init = function(RouterVersions) {
		App.use(BodyParser.urlencoded({
			extended: true
		}));
		App.use(BodyParser.json());
		App.use(Cors(GetCorsConfiguration()));
		App.use(ResourceConfig.init);

		SetRouterVersions(RouterVersions);
		NotFoundProvider.Handler(App);
		ErrorProvider.Init();
	};

	var _getApp = function() {
		return App;
	};

	var GetCorsConfiguration = function() {
		var CorsOptions = {};
		CorsOptions.origin = '*';
		CorsOptions.methods = '*';
		CorsOptions.allowedHeaders = '*';
		CorsOptions.optionsSuccessStatus = true;

		return CorsOptions;
	};

	var SetRouterVersions = function(RouterVersions) {
		var RouterVersionsKeys = Object.keys(RouterVersions);
		for (var i = 0; i < RouterVersionsKeys.length; ++i) {
			var VersionKey = RouterVersionsKeys[i];
			var Router = RouterVersions[VersionKey];
			
			App.use('/api/' + VersionKey + '/', Router);
		}
	};

	self.Init = _init;
	self.GetApp = _getApp;
})(this);