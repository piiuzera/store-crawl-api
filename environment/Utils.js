'use strict';

var FieldsModel = require('../enums/FieldsModel');
var FieldsRequest = require('../enums/FieldsRequest');
var HttpHeaders = require('../enums/HttpHeaders');
var HttpStatus = require('../enums/HttpStatus');
var JsonFile = require('jsonfile');
var Moment = require('moment');
var Validator = require('validatorjs');

(function(self) {

	var _addRoutes = function(Routes, baseRoute, method, uri, verifyAuthOrCallback, callbackOrNull) {
		var NewRoute = {};
		NewRoute.baseRoute = baseRoute;
		NewRoute.method = method;
		NewRoute.uri = uri;
		NewRoute.verifyAuth = callbackOrNull ? verifyAuthOrCallback : null;
		NewRoute.callback = callbackOrNull ? callbackOrNull : verifyAuthOrCallback;

		Routes.push(NewRoute);
	};

	var _getThrowException = function(message, code, details, status, retryOrNull) {
		var Exception = {};
		Exception.message = message;
		Exception.code = code;
		Exception.details = details;
		Exception.status = status ? status : HttpStatus.HTTP_STATUS_BAD_REQUEST;

		if (retryOrNull) {
			Exception.retry = true;
		}

		return Exception;
	};

	var _getValidatorThrowException = function(request, fields) {
		var Validation = new Validator(request, fields, GetRequiredAttributesMessage());
		if (Validation.fails()) {
			throw self.GetThrowException(__('FieldsNotInformed'), 'FieldsNotInformed', Validation.errors.errors);
		}
	};

	var _getRequestTestModel = function(Body, Query, Params, Headers) {
		var Request = {};
		Request.body = {};
		Request.query = {};
		Request.params = {};
		Request.headers = {};
		
		if (Body)
			Request.body = Body;

		if (Query)
			Request.query = Query;

		if (Params)
			Request.params = Params;

		if (Headers)
			Request.headers = Headers;

		return Request;
	};

	var _getRequestHeaderAuthorization = function(Token) {
		var RequestHeaders = {};
		RequestHeaders.authorization = Token;

		return RequestHeaders;
	};

	var _getResponseTestModel = function() {
		var Response = {};
		Response.statusCode = 0;
		Response.body = null;
		Response.status = function(statusCode) {
			var _json = function(body) {
				Response.body = body;
			};
			Response.statusCode = statusCode;

			return {
				json: _json
			};
		};

		return Response;
	};

	var _getNextTestModel = function() {
		this.body = {};
		this.body.success = true;
		this.body.date = new Date();
		this.body.status = HttpStatus.HTTP_STATUS_SUCCESS;
	};

	var _getHttpResponseModel = function() {
		var HttpResponse = {};
		HttpResponse.success = true;
		HttpResponse.date = new Date();
		HttpResponse.message = __('ResultSuccess');
		HttpResponse.status = HttpStatus.HTTP_STATUS_SUCCESS;

		return HttpResponse;
	};

	var _getHttpResponseErrorModel = function(status, exception) {
		var HttpResponse = {};
		HttpResponse.success = false;
		HttpResponse.date = new Date();
		HttpResponse.message = exception && exception.message ? exception.message : '';
		HttpResponse.code = exception && exception.code ? exception.code : '';
		HttpResponse.details = exception && exception.details ? exception.details : '';
		HttpResponse.status = status;

		return HttpResponse;
	};

	var _getReturnsError = function(HttpResponse, Error, Status) {
		HttpResponse.success = false;
		HttpResponse.message = Error.message;
		HttpResponse.code = Error.code;
		HttpResponse.details = Error.details;
		HttpResponse.status = Status;
		
		if (VerifyErrorConnectionRetry(HttpResponse.details) || Error.retry) {
			HttpResponse.retry = true;
		}

		return false;
	};

	var _getReturnsResponse = function(Request, Response, HttpResponse) {
		if (!HttpResponse.token) {
			HttpResponse.token = Request.headers.authorization;
		}
		if (Response.IsDeprecated) {
			HttpResponse.deprecated = __('DeprecatedMessage').replace(':version', process.env.APP_VERSION);
		}
		if (Request.headers[HttpHeaders.HEADER_TIMETRACKING_USE_MOCK] === 'true') {
			HttpResponse.isMock = true;
		}
		Response.status(HttpResponse.status).json(HttpResponse);
	};

	var _getResourceMessage = function(Language) {
		var ResourceFile = JsonFile.readFileSync(process.cwd() + '/strings/' + Language + '.json');
		return ResourceFile;
	};

	var _getCookieToObject = function(Cookies) {
		var CookieObject = {};
		for (var i = 0; i < Cookies.length; ++i) {
			var CookieStr = Cookies[i];
			var CookieSplit = CookieStr.split(';');
			for (var j = 0; j < CookieSplit.length; ++j) {
				var CookieObjectStr = CookieSplit[j];
				var CookieObjectSplit = CookieObjectStr.split('=');
				var ObjectKey = '';
				var ObjectValue = '';
				if (CookieObjectSplit.length > 2) {
					for (var i = 0; i < CookieObjectSplit.length; ++i) {
						if (i === 0) {
							ObjectKey = CookieObjectSplit[i];
						} else if (i === 1) {
							ObjectValue = CookieObjectSplit[i];
						} else if (CookieObjectSplit[i] === '') {
							ObjectValue += '=';
						} else {
							ObjectValue += CookieObjectSplit[i];
						}
					}
				} else {
					ObjectKey = CookieObjectSplit[0];
					ObjectValue = CookieObjectSplit[1];
				}

				CookieObject[ObjectKey] = ObjectValue;
			}
		}

		return CookieObject;
	};

	var _getObjectToCookie = function(Obj) {
		var Cookie = '';
		for (var Key in Obj) {
			Cookie += Key + '=' + Obj[Key] + ';';
		}

		return Cookie.substring(0, Cookie.length - 1);
	};

	var _jsonToQueryString = function(json) {
	    return Object.keys(json).map(function(key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(json[key]);
        }).join('&');
	};

	var _getStringToJsonFormat = function(jsonStr) {
		var JsonObject = {};
		eval('JsonObject = ' + jsonStr + ';');

		return JsonObject;
	};

	var _getStringToJsonFormatOrString = function(JsonStr) {
		try {
			var JsonObject = {};
			eval('JsonObject = ' + JsonStr + ';');

			return JsonObject;
		} catch (ex) {
			return JsonStr;
		}
	};

	var _getStringOrNull = function(str) {
		if (!str) {
			return null;
		} else if (str === '') {
			return null;
		} else if (str === 'Null') {
			return null;
		}
		return str;
	};

	var _getDateToMillis = function(date) {
		return Moment(date).toDate().getTime();
	};

	var _getDateFormat = function(date, format) {
		return Moment(date).format(format);
	};

	var _getDateStrToBetweenDatabaseDateFormat = function(date, isPlus) {
		var DateSplit = date.split('/');
		var Day = DateSplit[0];
		var Month = DateSplit[1];
		var Year = DateSplit[2];

		var DateObject = new Date(Year + '-' + Month + '-' + Day);
		DateObject = isPlus ? DateObject.addDays(2) : DateObject.addDays(0);

		return self.GetDateFormat(DateObject, 'YYYY-MM-DD');
	};

	var _generateRequestHash = function() {
		return Math.random().toString(16).slice(2);
	};

	var _getAuthenticationByHeaders = function(Headers) {
		var Authentication = {};
		Authentication[FieldsModel.USERNAME] = Headers[HttpHeaders.HEADER_CIANDT_USERNAME];
		Authentication[FieldsModel.PASSWORD] = Headers[HttpHeaders.HEADER_CIANDT_PASSWORD];

		return Authentication;
	};

	var _getAuthenticationByBody = function(Body) {
		var Authentication = {};
		Authentication[FieldsModel.USERNAME] = Body[FieldsRequest.USERNAME];
		Authentication[FieldsModel.PASSWORD] = Body[FieldsRequest.PASSWORD];

		return Authentication;
	};

	var _getLanguageByHeaders = function(Headers) {
		return Headers[HttpHeaders.ACCEPT_LANGUAGE];
	};

	var _getTimeMillis = function() {
		var DateObject = new Date();

		return DateObject.getTime();
	};

	var GetRequiredAttributesMessage = function() {
		var ValidationRequiredMessage = {};
		ValidationRequiredMessage.required 	= __('RequiredAttributes');
		ValidationRequiredMessage.confirmed = __('ConfirmedAttributes');
		ValidationRequiredMessage.email 		= __('EmailAttributes');

		return ValidationRequiredMessage;
	};

	var _stringValidOrEmpty = function(str) {
		if (str && str !== '') {
			return str.trim();
		}
		return '';
	};

	var _getFirstLetterUpperCase = function(StrText) {
		var TextObject = '';
		var StrTextSplit = StrText.split(' ');
		for (var i = 0; i < StrTextSplit.length; ++i) {
			var Text = StrTextSplit[i];
			var FirstLetterText = Text[0];
			var TextWithoutFirstLetter = Text.toLowerCase().substring(1, Text.length);

			TextObject += (FirstLetterText + TextWithoutFirstLetter + ' ');
		}

		return TextObject.trim();
	};

	var _jsonToModel = function(JsonObject, Model) {
		if (JsonObject) {
			return new Model(JsonObject);
		}
		return null;
	};

	var _jsonArrayToModelArray = function(JsonArray, Model) {
		var ModelArray = [];
		for (var i = 0; i < JsonArray.length; ++i) {
			var JsonObject = JsonArray[i];
			ModelArray.push(self.JsonToModel(JsonObject, Model).ToJson());
		}

		return ModelArray;
	};

	var _getTimeTrackingCalendar = function(StartDate, EndDate, TimeTrackingList) {
		var TimeTrackingCalendar = [];

		StartDate = self.StringToDate(StartDate);
		EndDate 	= self.StringToDate(EndDate);

		while(StartDate <= EndDate) {
			var Exist 				= false;
			for (var i = 0; i < TimeTrackingList.length; ++i) {
				var TimeTracking = TimeTrackingList[i];
				if (TimeTracking.date === self.GetDateFormat(StartDate, 'YYYY-MM-DD')) {
					Exist = true;
					TimeTracking.weekDay = __(self.GetLocaleWeekDay(StartDate.getDay()));
					TimeTrackingCalendar.unshift(TimeTracking);
				}
			}

			if (!Exist) {
				var NewTimeTracking 	= {};
				NewTimeTracking.date = self.GetDateFormat(StartDate, 'YYYY-MM-DD');
				NewTimeTracking.timetracking = [];
				NewTimeTracking.weekDay = __(self.GetLocaleWeekDay(StartDate.getDay()));
				TimeTrackingCalendar.unshift(NewTimeTracking);
			}
			StartDate = StartDate.addDays(1);
		}

		return TimeTrackingCalendar;
	};

	var _stringToDate = function(DateStr) {
		var DateSplit = DateStr.split('/');
		var Day 	= Number(DateSplit[0]);
		var Month = (Number(DateSplit[1]) - 1);
		var Year 	= Number(DateSplit[2]);

		return new Date(Year, Month, Day);
	};

	var _getStringQueryStringWhiteSpace = function(Str) {
		while(Str.indexOf(' ') !== -1) {
			Str = Str.replace(' ', '%20');
		}

		return Str;
	};

	var _getLocaleWeekDay = function(Weekday) {
		return ('WeekDay' + Weekday);
	};

	var _getUrl = function(Request) {
		return (Request.protocol + '://' + Request.headers.host);
	};

	var VerifyErrorConnectionRetry = function(Details) {
		if (Details && (typeof Details) === 'string') {
			return Details.indexOf('erroConnection') > -1;
		}
		return false;
	};

	Date.prototype.addDays = function(days) {
	    var date = new Date(this.valueOf());
	    date.setDate(date.getDate() + days);
	    return date;
	};

	self.AddRoute = _addRoutes;
	self.GetThrowException = _getThrowException;
	self.GetValidatorThrowException = _getValidatorThrowException;
	self.GetRequestTestModel = _getRequestTestModel;
	self.GetRequestHeaderAuthorization = _getRequestHeaderAuthorization;
	self.GetResponseTestModel = _getResponseTestModel;
	self.GetNextTestModel = _getNextTestModel;
	self.GetHttpResponseModel = _getHttpResponseModel;
	self.GetHttpResponseErrorModel = _getHttpResponseErrorModel;
	self.GetReturnsError = _getReturnsError;
	self.GetReturnsResponse = _getReturnsResponse;
	self.GetResourceMessage = _getResourceMessage;
	self.GetCookieToObject = _getCookieToObject;
	self.GetObjectToCookie = _getObjectToCookie;
	self.JsonToQueryString = _jsonToQueryString;
	self.GetStringToJsonFormat = _getStringToJsonFormat;
	self.GetStringToJsonFormatOrString = _getStringToJsonFormatOrString;
	self.GetStringOrNull = _getStringOrNull;
	self.GetDateToMillis = _getDateToMillis;
	self.GetDateFormat = _getDateFormat;
	self.GetDateStrToBetweenDatabaseDateFormat = _getDateStrToBetweenDatabaseDateFormat;
	self.GenerateRequestHash = _generateRequestHash;
	self.GetAuthenticationByHeaders = _getAuthenticationByHeaders;
	self.GetAuthenticationByBody = _getAuthenticationByBody;
	self.GetLanguageByHeaders = _getLanguageByHeaders;
	self.GetTimeMillis = _getTimeMillis;
	self.StringValidOrEmpty = _stringValidOrEmpty;
	self.GetFirstLetterUpperCase = _getFirstLetterUpperCase;
	self.JsonToModel = _jsonToModel;
	self.JsonArrayToModelArray = _jsonArrayToModelArray;
	self.GetTimeTrackingCalendar = _getTimeTrackingCalendar;
	self.GetStringQueryStringWhiteSpace = _getStringQueryStringWhiteSpace;
	self.StringToDate = _stringToDate;
	self.GetLocaleWeekDay = _getLocaleWeekDay;
	self.GetUrl = _getUrl;
})(this);