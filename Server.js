'use strict';

var EnvConfig = require('./environment/EnvConfig');
var Fork = require('./crawl/Fork');
var HttpServer = require('./environment/HttpServer');
var Log = require('./config/Log');

(function(self) {

	var _init = function() {
		var RouterVersions = EnvConfig.GetRouterVersion();

		StartServersRouterVersion(EnvConfig.GetStartServer(), RouterVersions);
		
		HttpServer.Init(RouterVersions);
		Fork.Init();

		HttpServer.GetApp().listen(
			process.env.APP_PORT,
			CallbackAppListen.bind(this, process.env.APP_PORT)
		);
	};

	var CallbackAppListen = function(AppPort) {
		Log.Info('SERVICE INITIALIZED IN PORT: ' + AppPort);
	};

	var StartServersRouterVersion = function(StartServers, RouterVersions) {
		var StartServersKeys = Object.keys(StartServers);
		for (var i = 0; i < StartServersKeys.length; ++i) {
			var ServerKey = StartServersKeys[i];

			StartServers[ServerKey].Init(RouterVersions[ServerKey], IsActualRouterVersion(ServerKey));
		}
	};

	var IsActualRouterVersion = function(ServerKey) {
		return ServerKey === process.env.APP_VERSION;
	};

	self.Init = _init;
})(this);

this.Init();